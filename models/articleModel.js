const Mongoose = require("mongoose");

let Schema = Mongoose.Schema;

let ArticleSchema = new Schema({
    titre: {type:String},
    categorie: {type:String},
    auteur: {type:String},
    date: {type:String},
    contenu: {type:String},
    image: {type:String}
}) 


var articleModel = Mongoose.model("ArticlesBlog", ArticleSchema);


/**********************partie API**************************** */
module.exports = {
    model :  articleModel, 
    getAllArticles : async() =>{
        return await articleModel.find({})
    },
    getArticleById : async(id) =>{
        return await articleModel.findById(id)
    },
    getArticleByCategorie : async(categorie) =>{
        return await articleModel.find({ categorie : categorie})
    },
    getArticleByAuteur : async(auteur) =>{
        return await articleModel.find({ auteur : auteur})
    },
    getArticleByDate : async(date) =>{
        return await articleModel.find({ date : date})
    },
    deleArticle :  async(id) =>{
        return await articleModel.findByIdAndRemove(id)
    },
    updateOneArticle: async(id, data) => {
        return await articleModel.updateOne({_id: id}, data)

    }

}

