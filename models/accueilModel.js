const Mongoose = require("mongoose");

var Schema = Mongoose.Schema;


let AccueilSchema = new Schema({
    titre: { type: String },
    contenu: { type: String },
    image: { type: String }
})

var accueilModel = Mongoose.model("ArticlesAccueil", AccueilSchema);

/**********************partie API**************************** */

module.exports = {
    model: accueilModel,
    getAllIntro: async () => {
        return await accueilModel.find({})
    },
    getIntroById: async (id) => {
        return await accueilModel.findById(id)
    },
    deleteIntro: async (id) => {
        return await accueilModel.findByIdAndRemove(id)
    },
    updateOneIntro: async (id, data) => {
        return await accueilModel.updateOne({ _id: id }, data)
    }
}