const Mongoose = require("mongoose");

var Schema = Mongoose.Schema;

let ServicesSchema = new Schema({
    titre: { type: String },
    contenu: { type: String },
    image: { type: String }
});

//exports.ServiceModel = Mongoose.model("ArticlesPageServices", ServicesSchema);

var serviceModel = Mongoose.model("ArticlesPageServices", ServicesSchema);

/**********************partie API**************************** */
module.exports = {
    model: serviceModel,
    getAllServices: async() => {
        return await serviceModel.find({})
    },
    getServiceById: async(id) => {
        return await serviceModel.findById(id)
    },
    updateOneService: async(id, data) => {
        return await serviceModel.updateOne({ _id: id }, data)
    },
    deleService: async(id) => {
        return await serviceModel.findByIdAndRemove(id)
    },
}

/* findOneAndUpdate()
findOneAndDelete()
useFindAndModify */