const Mongoose = require("mongoose");

var Schema = Mongoose.Schema;

let ContactSchema = new Schema({
    nom: { type: String },
    prenom: { type: String },
    courriel: { type: String },
    phone: { type: String },
    commentaire: { type: String }
})

exports.ContactModel = Mongoose.model("Contacts", ContactSchema)