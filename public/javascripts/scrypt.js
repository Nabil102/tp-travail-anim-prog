$(function () {
   $("#validationform").validate({
      rules: {
         nom: "required",
         prenom: "required",
         courriel: {
            required: true,
            email: true
         },
         phone: {
            matches: "[0-9]+",
            minlength: 10,
            maxlength: 10,
            required: true
         }
      },
      messages: {
         nom: "veuillez entrer votre nom",
         prenom: "veuillez entrer votre prenom",
         phone: {
            required: "veuillez entrer votre numero de telephone",
            matches: "veuillez entrer un numero de telephone valide",
            minlength: "veuillez entrer un numero de telephone valide",
            maxlength: "veuillez entrer un numero de telephone valide"
         },
         courriel: "veuillez entrer votre adresse courriel"
      },
      submitHandler: function (form) {
         form.submit();
      }
   });
});
