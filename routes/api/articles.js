var express = require('express');
var router = express.Router();
var articlesController = require("../../controllers/api/articlescontroller");

router.get('/', articlesController.getAllArticles);
router.get('/:id', articlesController.getArticleById);
router.get('/categorie/:categorie', articlesController.getArticleByCategorie);




module.exports = router; 