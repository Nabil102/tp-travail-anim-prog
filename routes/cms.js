let express = require('express');
let router = express.Router();
var multer = require("multer");
var cmsController = require("../controllers/cmsController");

/**************************************************
 * Configuration multer
 *
 ************************************************/

/** Enregistre les images dans le dossier images*******/

const storage = multer.diskStorage({
    destination: (requete, file, cb) => {
        cb(null, "./public/images")
    },
    /** attribue un nom aleatoire à l'image en utilisant la date*******/

    filename: (requete, file, cb) => {
        var date = new Date().toLocaleDateString();
        cb(null, Math.round(Math.random() * 10000) + "-" + file.originalname)
    }
});

/** Limite le format de l'image qu'on veut enregistrée en jpeg et png*******/

const fileFilter = (requete, file, cb) => {
    if (file.mimetype === "image/jpg" || file.mimetype === "image/png") {
        cb(null, true)
    } else {
        cb(new Error("l'image n'est pas acceptée"), false)
    }
}

/** Limite la taille de l'image enregistrée*******/

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
})

/**get des pages ***/

router.get('/', cmsController.index);
router.get('/accueil', cmsController.addArticleAccueil);


//router.get('/services', cmsController.addArticleServices);

router.get('/services/addService', cmsController.addArticleServices);
router.get('/services/allServices', cmsController.getServices);
router.get('/services/service/:id', cmsController.getServiceById);
router.get('/services/delete/:id', cmsController.deleteServiceByid);
router.get('/services/update/:id', cmsController.updateOneService);

router.get('/blog/addArticle', cmsController.addArticleBlog);
router.get('/blog/allArticles', cmsController.getArticles);
router.get('/blog/article/:id', cmsController.getArticleById);
router.get('/blog/delete/:id', cmsController.deleteArticleByid);
router.get('/blog/update/:id', cmsController.updateOneArticle);

router.get('/accueil/addAccueil', cmsController.addArticleAccueil);
router.get('/accueil/allAccueil', cmsController.getIntro);
router.get('/accueil/accueilS/:id', cmsController.getIntroById);
router.get('/accueil/delete/:id', cmsController.deleteIntroByid);
router.get('/accueil/update/:id', cmsController.updateOneIntro);


/**post des pages ***/

router.post('/accueil', upload.single("image"), cmsController.addSecctionAccueil);
router.post('/services', upload.single("image"), cmsController.addSecctionServices);

router.post('/blog/addArticle', upload.single("image"), cmsController.addArticle);
router.post('/blog/update/:id', upload.single("image"), cmsController.updateOneArticle);

router.post('/services/addService', upload.single("image"), cmsController.addSecctionServices);
router.post('/services/update/:id', upload.single("image"), cmsController.updateOneService);

router.post('/accueil/addAccueil', upload.single("image"), cmsController.addSecctionAccueil);
router.post('/accueil/update/:id', upload.single("image"), cmsController.updateOneIntro);



module.exports = router;