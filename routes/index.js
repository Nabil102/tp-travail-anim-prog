var express = require('express');
var router = express.Router();
var bolgcontroller = require('../controllers/blogcontroler')
var marketingcontroller = require("../controllers/marketingController.js");

/* pour validation contact */
const { body, validationResult } = require('express-validator');


/* GET des pages  */
router.get('/', marketingcontroller.getAccueil);
router.get('/services', marketingcontroller.getServices);
router.get('/api/api', marketingcontroller.apiRest);
router.get('/contact', marketingcontroller.contact);

/**************Routes de la page blog************ */
router.get('/blog', bolgcontroller.getAllArticles);
router.get('/blog/auteur/:auteur', bolgcontroller.getArticleAuteurs);
router.get('/blog/categorie/:categorie', bolgcontroller.getArticleByCategories);
router.get('/blog/date/:date', bolgcontroller.getArticleByDates);


/* POST des pages  */
router.post('/contact',
    body('nom').isLength({ min: 2 }),
    body('prenom').isLength({ min: 2 }),
    body('courriel').isEmail(),
    body('phone').isMobilePhone(),
    body('commentaire')
    .isLength({ min: 5 })
    .withMessage('Veuillez nous donner plus de détails s\'il vous plaît'),
    marketingcontroller.addContact);



module.exports = router;