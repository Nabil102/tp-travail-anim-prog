let express = require('express');
let router = express.Router();
var usersController = require("../controllers/usersController.js");


router.get('/', usersController.index);
router.get('/adduser', usersController.confirmation);
router.get('/pagescms', usersController.mespagescms);
router.get('/login', usersController.login);





router.post('/adduser', usersController.registerUser);
router.post('/login', usersController.loginUser);

module.exports = router;