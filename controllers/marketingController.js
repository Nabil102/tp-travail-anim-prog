var ModelArticle = require('../models/articleModel.js');
var ModelService = require('../models/serviceModel.js');
var AccueilModel = require('../models/accueilModel.js');
var { ContactModel } = require('../models/contactModel.js');

/* pour validation contact en back end */
const { validationResult } = require('express-validator');

exports.getAccueil = async(req, res, next) => {
    try {
        let articleAccueil = await AccueilModel.getAllIntro();
        res.render('index', { articlesAccueil: articleAccueil });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getServices = async(req, res, next) => {
    try {
        let service = await ModelService.getAllServices();
        res.render('services', { services: service });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}


exports.apiRest = async(req, res, next) => {
    res.render('api/api', { title: 'API Rest' });
}

exports.contact = (req, res, next) => {
    res.render('contact');
}



exports.addContact = async(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    } else {

        let data = new ContactModel({
            nom: req.body.nom,
            prenom: req.body.prenom,
            courriel: req.body.courriel,
            phone: req.body.phone,
            commentaire: req.body.commentaire
        });
        console.log(req.body)
        data.save(function(err, data) {
            if (err) console.error(err);
            res.redirect('/contact')
        })
    }
}