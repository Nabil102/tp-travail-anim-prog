const ArticlesModel = require('../models/articleModel.js')


module.exports ={
    getAllArticles : async (req, res, next)=>{
        try{
            let articles = await ArticlesModel.getAllArticles()
            res.render('blog/blog', { article: articles });
        }catch (err){
            console.log(err);
            res.status(500).send('aucun articles trouvés')
        }
    },
    getArticleByCategories : async (req, res, next) => {
        try{
            let articleCategories = await ArticlesModel.getArticleByCategorie(req.params.categorie)
            res.render( 'blog/categories', {articleCategorie: articleCategories })
        }catch (err){
            console.log(err);
            res.status(500).send('Article non trouvé')
    }
},
getArticleAuteurs : async (req, res, next) => {
    try{
        let articleAuteurs= await ArticlesModel.getArticleByAuteur(req.params.auteur)
        res.render('blog/auteurs', {articleAuteur: articleAuteurs });
    }catch (err){
        console.log(err);
        res.status(500).send('Article non trouvé')
}
},
getArticleByDates : async (req, res, next) => {
    try{
        let articleAuteursDates = await ArticlesModel.getArticleByDate(req.params.date)
        res.render('blog/dates', {articleAuteursDate: articleAuteursDates });
    }catch (err){
        console.log(err);
        res.status(500).render('Article non trouvé')
}
},
}