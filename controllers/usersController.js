const { UserModel } = require('../models/utilisateurModel.js');
const bcrypt = require('bcrypt')

exports.index = (req,res,next) =>{
    res.render('users/adduser');
}

exports.login = (req,res,next) =>{
    res.render('users/login');
}
exports.confirmation = (req,res,next) =>{
    res.render('users/confirmation');
}

exports.mespagescms = (req, res, next) => {
    res.render('users/pagescms');
}

exports.registerUser = async (req,res,next) =>{
    try{
        const hashed_password = await bcrypt.hash(req.body.password, 10);
        const user = new UserModel({
            name: req.body.name, 
            email: req.body.email,
            password: hashed_password
        });
        user.save((err, document)=>{
            if (err) res.render('users/adduser', {error:err});
            res.render('users/confirmation', {data:document});
        });
    } catch(err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.loginUser = async (req, res,next) =>{
    try{
        const user = await UserModel.findOne({email:req.body.email});
        console.log(req.body.email)
        if(user){
            let compare = await bcrypt.compare(req.body.password, user.password); 
            if(compare){
                res.render('users/pagescms', {title:"Bienvenue! Au gestion de contenu Marketing Web", username: user.name, layout: 'cmslayout'})
            } else {
                res.render('users/login',{message:"Vous n'êtes pas encore enregistré."});
            }
        } else {
            res.render('users/login',{message:"Mauvais nom d'utilisateur ou mot de passe."});
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}