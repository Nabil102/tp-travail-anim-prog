var ModelArticle = require('../models/articleModel.js');
const { ServiceModel } = require('../models/serviceModel.js');
const accueilModel = require('../models/accueilModel.js');
//const { ServiceModel } = require('../models/serviceModel.js');
var AccueilModel = require('../models/accueilModel.js');
var ModelService = require('../models/serviceModel.js');


exports.index = (req, res, next) => {
    res.render('cms/index');
}


/**************** Pour ajouter un article au blog****************8 */

exports.addArticleBlog = (req, res, next) => {
    res.render("cms/blog/addArticle");
}
exports.addArticle = async (req, res, next) => {
    let data = new ModelArticle.model({
        titre: req.body.titre,
        categorie: req.body.categorie,
        auteur: req.body.auteur,
        date: req.body.date,
        contenu: req.body.contenu,
        image: req.file.path.substring(14)
    });
    console.log(req.body)
    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/blog')
    })
}

exports.getArticles = async (req, res, next) => {
    try {
        let document = await ModelArticle.getAllArticles();
        res.render('cms/blog/allArticles', { article: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getArticle = (req, res, next) => {
    res.render("cms/blog/article");
}

exports.getArticleById = async (req, res, next) => {
    try {
        let document = await ModelArticle.getArticleById(req.params.id);
        res.render('cms/blog/article', { article: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

/*********pour supprimer un article du blog **************** */


exports.deleteArticleByid = async (req, res, next) => {
    try {
        ModelArticle.deleArticle(req.params.id)
        res.redirect('/cms/blog/allArticles');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateOneArticle = async (req, res, next) => {
    try {
        let data = {
            titre: req.body.titre,
            categorie: req.body.categorie,
            auteur: req.body.auteur,
            date: req.body.date,
            contenu: req.body.contenu,
        }
        await ModelArticle.updateOneArticle(req.body.id, data)
        res.status(200).send(`Article id ${article_id} mise à jour`);
    } catch (err) {
        console.error(err);
        res.redirect('/cms/blog/allArticles')
    }
}

/** page accueil**/

exports.addSecctionAccueil = async (req, res, next) => {
    let data = new AccueilModel({
        titre: req.body.titre,
        contenu: req.body.contenu,
        image: req.file.path.substring(14)
    });
    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/')
    })
}


exports.addArticleAccueil = (req, res, next) => {
    res.render("cms/accueil/addAccueil");
}

exports.getIntroById = async (req, res, next) => {
    try {
        let document = await AccueilModel.getIntroById(req.params.id);
        res.render('cms/accueil/accueilS', { articlesAccueil: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.addSecctionAccueil = async (req, res, next) => {
    let data = new AccueilModel.model({
        titre: req.body.titre,
        contenu: req.body.contenu,
        image: req.file.path.substring(14)
    });
    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/')
    })
}

exports.getIntro = async (req, res, next) => {
    try {
        let document = await AccueilModel.getAllIntro();
        console.log(document)
        res.render('cms/accueil/allAccueil', { articlesAccueil: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}


exports.deleteIntroByid = async (req, res, next) => {
    try {
        AccueilModel.deleteIntro(req.params.id)
        res.redirect('/');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateOneIntro = async (req, res, next) => {
    try {
        let data = {
            titre: req.body.titre,
            contenu: req.body.contenu,
        }
        await AccueilModel.updateOneIntro(req.body.id, data)
        console.log(req.body.id)
        res.status(200).send(`Intro id ${Intro_id}​​​​​ mise à jour`);
    } catch (err) {
        console.error(err);
        res.redirect('/')
    }
}


/* ******** page services ******** */

/* ** Ajoute un service à la page services ** */

exports.addArticleServices = (req, res, next) => {
    res.render("cms/services/addService");
}

exports.addSecctionServices = async (req, res, next) => {
    console.log(req)
    let data = new ModelService.model({
        titre: req.body.titre,
        contenu: req.body.contenu,
        image: req.file.path.substring(14)
    });

    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/services')
    })
}

exports.getServices = async (req, res, next) => {
    try {
        let document = await ModelService.getAllServices();
        res.render('cms/services/allServices', { service: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getService = (req, res, next) => {
    res.render("cms/services/service");
}

exports.getServiceById = async (req, res, next) => {
    try {
        let document = await ModelService.getServiceById(req.params.id);
        res.render('cms/services/service', { service: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

/*********pour supprimer un service de la page service **************** */

exports.deleteServiceByid = async (req, res, next) => {
    try {
        ModelService.deleService(req.params.id)
        res.redirect('/cms/services/allServices');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

/* exports.updateOneService = async (req, res, next) => {
    let data = ServiceModel.model({
        titre: req.body.titre,
        contenu: req.body.contenu,
    });
    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/cms/services/allServices')
    })
}  */

exports.updateOneService = async (req, res, next) => {
    try {
        let data = {
            titre: req.body.titre,
            contenu: req.body.contenu,
        };
        await ModelService.updateOneService(req.body.id, data);
        res.status(200).send(`Service id ${service_id} mise à jour`);
    } catch (err) {
        console.error(err);
        res.redirect('/cms/services/allServices');
    }
}