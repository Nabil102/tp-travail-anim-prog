const ArticlesModel = require('../../models/articleModel')

module.exports ={
    getAllArticles : async (req, res, next)=>{
        try{
            let articles = await ArticlesModel.getAllArticles();
            res.json(articles)
        }catch (err){
            console.log(err);
            res.status(500).send('aucun articles trouvés')
        }
    },
    /*******rtourne un article by id************* */
    getArticleById : async (req, res, next) =>{
        try{
            let article = await ArticlesModel.getArticleById(req.params.id)
            res.json(article)
        }catch (err){
            console.log(err);
            res.status(500).send('Article non trouvé')
    }
}, 
    getArticleByCategorie : async (req, res, next) => {
        try{
            let article = await ArticlesModel.getArticleByCategorie(req.params.categorie)
            res.json(article)
        }catch (err){
            console.log(err);
            res.status(500).send('Article non trouvé')
    }
}
}
